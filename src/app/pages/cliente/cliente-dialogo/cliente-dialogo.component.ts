import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Cliente } from 'src/app/_model/cliente';
import { ClienteService } from 'src/app/_service/cliente.service';
import { DomSanitizer } from '@angular/platform-browser';
import { debug } from 'util';
import * as moment from 'moment';
import { UsuarioService } from 'src/app/_service/usuario.service';
import { Usuario } from 'src/app/_model/usuario';

@Component({
  selector: 'app-cliente-dialogo',
  templateUrl: './cliente-dialogo.component.html',
  styleUrls: ['./cliente-dialogo.component.css']
})
export class ClienteDialogoComponent implements OnInit {


  cliente: Cliente;
  usuario: Usuario;
  imagenData: any;
  imagenEstado: boolean = false;
  selectedFiles: FileList;
  currentFileUpload: File;
  labelFile: string;
  nuevo: boolean;
  confirmaClave: string;

  constructor(
    private dialogRef: MatDialogRef<ClienteDialogoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Cliente,
    private clienteService: ClienteService,
    private usuarioService: UsuarioService,
    private sanitization: DomSanitizer
  ) { }

  ngOnInit() {
    this.cliente = new Cliente();
    this.usuario = new Usuario();

    // debugger;
    if (this.data.idCliente > 0) {
      this.usuarioService.listarPorId(this.data.idCliente).subscribe( x => {
        this.cliente.idCliente = x['cliente'].idCliente;
        this.cliente.apellidos = x['cliente'].apellidos;
        this.cliente.nombres = x['cliente'].nombres;
        this.cliente.fechaNac =  moment(x['cliente'].fechaNac).utc().toDate();
        this.cliente.dni = x['cliente'].dni;
        this.usuario.nombre = x['nombre'];
      });

      this.clienteService.fotoPorId(this.data.idCliente).subscribe(data => {
        if (data.size > 0) {
          this.convertir(data);
        }
      });
      this.nuevo = false;
    } else {
      this.nuevo = true;

    }
  }

  convertir(data: any) {
    let reader = new FileReader();
    reader.readAsDataURL(data);
    reader.onloadend = () => {
      let base64 = reader.result;
      this.setear(base64);
    }
  }

  setear(x: any) {
    this.imagenData = this.sanitization.bypassSecurityTrustResourceUrl(x);
    this.imagenEstado = true;
  }

  operar() {
    this.cliente.usuario = this.usuario;

    if (this.selectedFiles != null) {
      this.currentFileUpload = this.selectedFiles.item(0);
    } else {
      this.currentFileUpload = new File([""], 'blanco');
    }

    if ( this.validateData() === true ) {
        if ( this.usuario.clave === this.confirmaClave ) {
            if (this.cliente != null && this.cliente.idCliente > 0) {
              this.clienteService.modificar(this.cliente, this.currentFileUpload).subscribe(data => {
                this.clienteService.listar().subscribe(clientes => {
                  this.clienteService.clienteCambio.next(clientes);
                  this.clienteService.mensajeCambio.next('Se modifico');
                });
              });
              this.dialogRef.close();
            } else {
                this.clienteService.registrar(this.cliente, this.currentFileUpload).subscribe(data => {
                  this.clienteService.listar().subscribe(clientes => {
                    this.clienteService.clienteCambio.next(clientes);
                    this.clienteService.mensajeCambio.next('Se registro');
                  });
                });
                this.dialogRef.close();
            }
        } else {
          this.clienteService.mensajeCambio.next('Las claves no coinciden');
        }
    }

  }

  validateData(): boolean {
    if (this.cliente.nombres === undefined) {
      this.clienteService.mensajeCambio.next('Debe de ingresar el Nombre');
      return false;
    }
    if (this.cliente.apellidos === undefined) {
      this.clienteService.mensajeCambio.next('Debe de ingresar el Apellido');
      return false;
    }
    if (this.cliente.dni === undefined) {
      this.clienteService.mensajeCambio.next('Debe de ingresar el DNI');
      return false;
    }
    if (this.cliente.fechaNac === undefined) {
      this.clienteService.mensajeCambio.next('Debe de ingresar la Fecha Nacimiento');
      return false;
    }
    if (this.usuario.nombre === undefined) {
      this.clienteService.mensajeCambio.next('Debe de ingresar el Usuario');
      return false;
    }
    return true;
  }

  selectFile(e: any) {
    console.log(e);
    this.labelFile = e.target.files[0].name;
    this.selectedFiles = e.target.files;
  }

  cancelar() {
    this.dialogRef.close();
  }


}
